<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu items grid
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Block_Adminhtml_Menu_Edit_Tab_Items
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('cmsMenuItemsGrid');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Efumo_MenuManager_Block_Adminhtml_Menu_Edit_Tab_Items
     */
    protected function _prepareCollection()
    {
        /* @var $collection Efumo_MenuManager_Model_Resource_Item_Collection */
        $collection = Mage::getModel('efumo_menumanager/item')->getResourceCollection()
            ->addMenuFilter(Mage::registry('menumanager_menu'));
        if (!$this->getRequest()->getParam('sort')) { $collection->setPositionOrder(); }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     * @return Efumo_MenuManager_Block_Adminhtml_Menu_Edit_Tab_Items
     */
    protected function _prepareColumns()
    {
        /* @var $model Efumo_MenuManager_Model_Menu*/
        $menuModel = Mage::registry('menumanager_menu');

        /* @var $model Efumo_MenuManager_Model_Item*/
        $ItemModel = Mage::getModel('efumo_menumanager/item');

        $this->addColumn('item_title', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Title'),
            'index'     => 'title',
        ));

        $this->addColumn('item_parent_id', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Parent'),
            'index'     => 'parent_id',
            'type'      => 'options',
            'renderer'  => 'Efumo_MenuManager_Block_Adminhtml_Menu_Edit_Tab_Renderer_Parent',
            'options'   => $ItemModel->getCollection()
                ->addMenuFilter($menuModel)
                ->toItemOptionArray(),
        ));

        $this->addColumn('item_url', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Url'),
            'index'     => 'url',
        ));

        $this->addColumn('item_mobile_url', array(
                'header'    => Mage::helper('efumo_menumanager')->__('Mobile Url'),
                'index'     => 'mobile_url',
        ));

        $this->addColumn('item_type', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Type'),
            'index'     => 'type',
            'type'      => 'options',
            'options'   => $ItemModel->getAvailableTypes(),
        ));

        $this->addColumn('item_css_class', array(
            'header'    => Mage::helper('efumo_menumanager')->__('CSS Class'),
            'index'     => 'css_class',
        ));

        $this->addColumn('item_position', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Position'),
            'index'     => 'position',
        ));

        $this->addColumn('dropdown_position', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Menu Position'),
            'index'     => 'dropdown_position',
            'type'      => 'options',
            'options'   => Mage::getSingleton('efumo_menumanager/adminhtml_form_positions')->toArray(),
        ));

        $this->addColumn('item_is_active', array(
            'header'    => Mage::helper('efumo_menumanager')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('efumo_menumanager')->__('Disabled'),
                1 => Mage::helper('efumo_menumanager')->__('Enabled'),
            ),
        ));

        return parent::_prepareColumns();
    }

    /**
     * Return row url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit_item', array(
            'item_id' => $row->getId(),
            'active_tab' => 'menu_page_tabs_items_section',
            'menu_id' => $this->getRequest()->getParam('menu_id'),
        ));
    }
}
