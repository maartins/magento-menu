<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu item edit form container
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Block_Adminhtml_Menu_Item_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'item_id';
        $this->_blockGroup = 'efumo_menumanager';
        $this->_controller = 'adminhtml_menu_item';

        parent::__construct();

        $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->_getBackUrl() . '\')');
        $this->_updateButton('delete', 'onclick', 'setLocation(\'' . $this->_getDeleteUrl() . '\')');
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('menumanager_menu_item')->getId()) {
            return Mage::helper('efumo_menumanager')->__("Edit Menu Item '%s'",
                $this->escapeHtml(Mage::registry('menumanager_menu_item')->getTitle()));
        } else {
            return Mage::helper('efumo_menumanager')->__('New Menu Item');
        }
    }

    /**
     * get header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-cms-block ' . strtr($this->_controller, '_', '-');
    }

    /**
     * Getter of url for "Back" button
     *
     * @return string
     */
    protected function _getBackUrl()
    {
        return $this->getUrl('*/*/edit', array(
            'menu_id' => $this->getRequest()->getParam('menu_id'),
            '_current' => true
        ));
    }

    /**
     * Getter of url for "Delete" button
     *
     * @return string
     */
    protected function _getDeleteUrl()
    {
        return $this->getUrl('*/*/delete_item', array(
            'menu_id' => $this->getRequest()->getParam('menu_id'),
            'item_id' => $this->getRequest()->getParam('item_id')
        ));
    }
}