<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu item edit form
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Block_Adminhtml_Menu_Item_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        /* @var $model Efumo_MenuManager_Model_Item*/
        $model = Mage::registry('menumanager_menu_item');
        $menuId = $this->getRequest()->getParam('menu_id');

        $form = new Varien_Data_Form(array(
            'method' => 'post',
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save_item', array('menu_id' => $menuId)),
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => Mage::helper('efumo_menumanager')->__('General Information'), )
        );

        if ($model->getItemId()) {
            $fieldset->addField('item_id', 'hidden', array(
                'name'  => 'item_id',
            ));
        }

        if ($model->getMenuId()) {
            $fieldset->addField('menu_id', 'hidden', array(
                'name'  => 'menu_id',
            ));
        }

        if ($model->getIdentifier()) {
            $fieldset->addField('identifier', 'hidden', array(
                'name' => 'identifier',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('efumo_menumanager')->__('Title'),
            'title'     => Mage::helper('efumo_menumanager')->__('Title'),
            'required'  => true,
        ));

        $fieldset->addField('parent_id', 'select', array(
            'name'      => 'parent_id',
            'label'     => Mage::helper('efumo_menumanager')->__('Parent'),
            'title'     => Mage::helper('efumo_menumanager')->__('Parent'),
            'options'   => $model->getCollection()
                ->addMenuFilter($menuId)
                ->toItemOptionArray(),
            'required'  => true,
        ));

        $fieldset->addField('url', 'text', array(
            'name'      => 'url',
            'label'     => Mage::helper('efumo_menumanager')->__('Url'),
            'title'     => Mage::helper('efumo_menumanager')->__('Url'),
            'note'      => Mage::helper('cms')->__('Use " / " For Item With Base Url.'),
        ));

        $fieldset->addField('mobile_url', 'text', array(
            'name'      => 'mobile_url',
            'label'     => Mage::helper('efumo_menumanager')->__('Mobile Url'),
            'title'     => Mage::helper('efumo_menumanager')->__('Mobile Url'),
            'note'      => Mage::helper('cms')->__('Use " / " For Item With Base Url.'),
        ));

        $fieldset->addField('type', 'select', array(
            'name'      => 'type',
            'label'     => Mage::helper('efumo_menumanager')->__('Url Window Type'),
            'title'     => Mage::helper('efumo_menumanager')->__('Url Window Type'),
            'options'   => $model->getAvailableTypes(),
            'required'  => true,
        ));

        $fieldset->addField('css_class', 'text', array(
            'name'      => 'css_class',
            'label'     => Mage::helper('efumo_menumanager')->__('CSS Class'),
            'title'     => Mage::helper('efumo_menumanager')->__('CSS Class'),
            'note'      => Mage::helper('cms')->__('Space Separated Class Names'),
        ));

        $fieldset->addField('position', 'text', array(
            'name'      => 'position',
            'label'     => Mage::helper('efumo_menumanager')->__('Position'),
            'title'     => Mage::helper('efumo_menumanager')->__('Position'),
            'class'     => 'validate-number',
            'required'  => true,
        ));

        $fieldset->addField('display_all', 'select', array(
            'label'     => Mage::helper('efumo_menumanager')->__('Display All'),
            'title'     => Mage::helper('efumo_menumanager')->__('Display All Link'),
            'name'      => 'display_all',
            'required'  => true,
            'options'   => array(
                '3' => Mage::helper('efumo_menumanager')->__('Show on side menu but not on mobile'),
                '2' => Mage::helper('efumo_menumanager')->__('Show on mobile but not on side menu'),
                '1' => Mage::helper('efumo_menumanager')->__('Show on mobile and on side menu'),
                '0' => Mage::helper('efumo_menumanager')->__('No'),
            ),
        ));

        $fieldset->addField('disable_category_sidebar', 'select', array(
            'label'     => Mage::helper('efumo_menumanager')->__('Hide From Category Sidebar'),
            'title'     => Mage::helper('efumo_menumanager')->__('Hide From Category Sidebar'),
            'name'      => 'disable_category_sidebar',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('efumo_menumanager')->__('Yes'),
                '0' => Mage::helper('efumo_menumanager')->__('No'),
            ),
        ));

        if (is_null($model->getId()) || $model->getParentId() != 0) {
            $fieldset->addField('dropdown_position', 'select', array(
                'name'      => 'dropdown_position',
                'label'     => Mage::helper('efumo_menumanager')->__('Position in menu'),
                'title'     => Mage::helper('efumo_menumanager')->__('Position in menu'),
                'options'   => Mage::getSingleton('efumo_menumanager/adminhtml_form_positions')->toArray(),
                'required'  => true,
            ));
        }

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('efumo_menumanager')->__('Status'),
            'title'     => Mage::helper('efumo_menumanager')->__('Menu Item Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('efumo_menumanager')->__('Enabled'),
                '0' => Mage::helper('efumo_menumanager')->__('Disabled'),
            ),
        ));

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        Mage::dispatchEvent(
            'adminhtml_cms_menu_item_edit_prepare_form',
            array('form' => $form)
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
