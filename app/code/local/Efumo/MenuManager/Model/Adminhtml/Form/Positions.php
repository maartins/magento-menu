<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu positions model
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Model_Adminhtml_Form_Positions
{
    public function toArray()
    {
        return array(
            0  => Mage::helper('adminhtml')->__('Right'),
            1  => Mage::helper('adminhtml')->__('Left'),
        );
    }
}
