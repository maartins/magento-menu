<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu item model
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Model_Item extends Mage_Core_Model_Abstract
{
    /**
     * Menu item url open window types
     */
    const TYPE_NEW_WINDOW = 'new_window';
    const TYPE_SAME_WINDOW = 'same_window';

    protected function _construct()
    {
        $this->_init('efumo_menumanager/item');
    }

    /**
     * Prepare menu item url open window types
     *
     * @return array
     */
    public function getAvailableTypes()
    {
        $types = array(
            self::TYPE_SAME_WINDOW => Mage::helper('efumo_menumanager')->__('Same Window'),
            self::TYPE_NEW_WINDOW => Mage::helper('efumo_menumanager')->__('New Window'),
        );

        return $types;
    }
}
