<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu model
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Model_Menu extends Mage_Core_Model_Abstract
{
    /**
     * Menu types
     */
    const TYPE_HORIZONTAL = 'horizontal';
    const TYPE_VERTICAL = 'vertical';
    const TYPE_NONE = 'none';

    /**
     * Cache tag
     */
    const CACHE_TAG = 'menumanager_menu';

    protected function _construct()
    {
        $this->_init('efumo_menumanager/menu');
    }

    /**
     * Prepare menu types
     *
     * @return array
     */
    public function getAvailableTypes()
    {
        $types = array(
            self::TYPE_NONE => Mage::helper('efumo_menumanager')->__('None'),
            self::TYPE_VERTICAL => Mage::helper('efumo_menumanager')->__('Vertical'),
            self::TYPE_HORIZONTAL => Mage::helper('efumo_menumanager')->__('Horizontal'),
        );

        return $types;
    }
}
