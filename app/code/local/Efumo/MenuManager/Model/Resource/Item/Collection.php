<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * MenuManager menu item collection
 *
 * @category    Efumo
 * @package     Efumo_MenuManager
 */
class Efumo_MenuManager_Model_Resource_Item_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('efumo_menumanager/item');
    }

    /**
     * Add menu filter to item collection
     *
     * @param   int | Efumo_MenuManager_Model_Menu $menu
     * @return  Efumo_MenuManager_Model_Resource_Item_Collection
     */
    public function addMenuFilter($menu)
    {
        if ($menu instanceof Efumo_MenuManager_Model_Menu) {
            $menu = $menu->getId();
        }

        $this->addFilter('menu_id', $menu);

        return $this;
    }

    /**
     * Add status filter to item collection
     *
     * @return  Efumo_MenuManager_Model_Resource_Item_Collection
     */
    public function addStatusFilter()
    {
        $this->addFilter('is_active', 1);

        return $this;
    }

    /**
     * Set order to item collection
     *
     * @return Efumo_MenuManager_Model_Resource_Item_Collection
     */
    public function setPositionOrder()
    {
        $this->setOrder('parent_id', 'asc');
        $this->setOrder('dropdown_position', 'asc');
        $this->setOrder('position', 'asc');

        return $this;
    }

    /**
     * Collection to option array method
     *
     * @return array
     */
    public function toItemOptionArray()
    {
        $result = array();
        $result['0'] = Mage::helper('efumo_menumanager')->__('Root');

        foreach ($this as $item) {
            $result[$item->getData('item_id')] = $item->getData('title');
        }

        return $result;
    }
}