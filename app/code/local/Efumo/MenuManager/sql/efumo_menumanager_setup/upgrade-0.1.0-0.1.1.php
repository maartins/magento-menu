<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create new column where to store dropdown position
 */

$table = $installer->getTable('efumo_menumanager/menu_item');
$newColumn = 'dropdown_position';
$coreRead = $installer->getConnection('core_read');
$dbName = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

$rows = $coreRead->fetchAll(<<<EOF
SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table' AND COLUMN_NAME = '$newColumn' AND TABLE_SCHEMA = '$dbName';
EOF
);

if (count($rows)) {
$installer->run("ALTER TABLE $table DROP COLUMN `$newColumn`;");
}

$installer->run(<<<EOF
ALTER TABLE $table ADD COLUMN `$newColumn` TINYINT NOT NULL DEFAULT 0 COMMENT 'Item position in menu' AFTER position;
EOF
);

$installer->endSetup();