<?php
/**
 * Efumo_MenuManager
 *
 * @category Efumo
 * @package Efumo_MenuManager
 * @author Efumo
 * @copyright Copyright (c) 2016 Efumo, Ltd (http://efumo.lv)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create new column where to store dropdown position
 */
$table = $installer->getTable('efumo_menumanager/menu_item');
$newColumn = 'mobile_url';
$coreRead = $installer->getConnection('core_read');
$dbName = (string) Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

$installer->run(<<<EOF
ALTER TABLE $table ADD COLUMN `$newColumn` varchar(255) NULL COMMENT 'Item url on mobile devices' AFTER url;
EOF
);

$installer->endSetup();
